<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>city</title>
    <style>

    </style>
    <link rel="stylesheet" href="assetc/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="table-responsive">
        <table border="2" class="table table-bordered">
            <tr>
                <td>Serial</td>
                <td>ID</td>
                <td>City</td>
                <td>Action</td>
            </tr>
            <tr>
                <td>1</td>
                <td>5</td>
                <td>Chittagong</td>
                <td><input class="btn btn-info" type="button" value="View">
                    <input class="btn btn-primary" type="button" value="Edit">
                    <input class="btn btn-danger" type="button" value="Delete">
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>6</td>
                <td>Dhaka</td>
                <td>
                    <input class="btn btn-info" type="button" value="View">
                    <input class="btn btn-primary" type="button" value="Edit">
                    <input class="btn btn-danger" type="button" value="Delete">
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>7</td>
                <td>Rajshahi</td>
                <td>
                    <input class="btn btn-info" type="button" value="View">
                    <input class="btn btn-primary" type="button" value="Edit">
                    <input class="btn btn-danger" type="button" value="Delete">
                </td>
            </tr>
            <tr>
                <td colspan="5"> PAGE < <a href="#"></a>
                    <a href="#">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <a href="#">6</a>
                    <a href="#">7</a> >
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <button type="button" class="btn btn-success">Add New City</button>
                    <button type="button" class="btn btn-info">Delete All</button>
                </td>
            </tr>

        </table>
    </div>
</div>


</body>

</html>